﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy_Pattern
{
    public class ImportExcel : ImportMethod
    {
        public string Import()
        {
            return "This is Import Excel";
        }
    }
}
