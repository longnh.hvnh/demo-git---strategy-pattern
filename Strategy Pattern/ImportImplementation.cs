﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy_Pattern
{
    public class ImportImplementation
    {
        private readonly ImportMethod importMethod;

        public ImportImplementation(ImportMethod importMethod)
        {
            this.importMethod = importMethod;
        }

        public string GetImportMethod()
        {
            return importMethod.Import();
        }
    }
}
